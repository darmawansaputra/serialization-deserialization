using System;

namespace Serialization_Deserialization {
    [Serializable]
    public class Enemy : Character {
        private double reward;

        public Enemy(int _level, float _health, string _name, double _reward) : base(_level, _health, _name) {
            reward = _reward;
        }

        public override void Hi() {
            Console.WriteLine("Hi im " + name + ", my reward is " + reward);
        }
    }
}
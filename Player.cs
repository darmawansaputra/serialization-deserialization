using System;

namespace Serialization_Deserialization {
    [Serializable]
    public class Player : Character {
        private int point;

        public Player(int _level, float _health, string _name, int _point) : base(_level, _health, _name) {
            point = _point;
        }

        public override void Hi() {
            Console.WriteLine("Hi im " + name + ", my point is " + point);
        }
    }
}
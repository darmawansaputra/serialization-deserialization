﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace Serialization_Deserialization {
    class Program {
        static void Main(string[] args) {
            Console.WriteLine("= Before Serialization =");

            // Creating player and enemy object
            Player player = new Player(1, 100, "Darmawan", 700);
            Enemy enemy = new Enemy(5, 500, "Garaga Enemy", 25.7);

            // Output player and enemy object
            OutputObject(player);
            OutputObject(enemy);


            Console.WriteLine("\n= After Serialization =");

            // Serialize player and enemy object to byte
            byte[] playerByte = Serialize(player);
            byte[] enemyByte = Serialize(enemy);

            // Output player and enemy byte
            Console.WriteLine("Player: ");
            OutputByte(playerByte);

            Console.WriteLine("\nEnemy: ");
            OutputByte(enemyByte);


            Console.WriteLine("\n= After Deserialization =");

            // Deserialize player and enemy byte to object again
            Character samePlayer = Deserialize(playerByte);
            Character sameEnemy = Deserialize(enemyByte);

            // Output same player and enemy object after deserialization
            OutputObject(samePlayer);
            OutputObject(sameEnemy);
        }

        static byte[] Serialize(Object _character) {
            BinaryFormatter bf = new BinaryFormatter();
            using (var ms = new MemoryStream()) {
                bf.Serialize(ms, _character);
                return ms.ToArray();
            }
        }

        static Character Deserialize(byte[] _byteCharacter) {
            using (var memStream = new MemoryStream()) {
                var binForm = new BinaryFormatter();
                memStream.Write(_byteCharacter, 0, _byteCharacter.Length);
                memStream.Seek(0, SeekOrigin.Begin);

                var obj = binForm.Deserialize(memStream);
                return (Character)obj;
            }
        }

        static void OutputObject(Character _character) {
            _character.Hi();
        }

        static void OutputByte(byte[] _characterByte) {
            Console.WriteLine("new byte[] { " + string.Join(", ", _characterByte) + " }");
        }
    }
}

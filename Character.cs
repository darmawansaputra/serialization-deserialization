using System;

namespace Serialization_Deserialization {
    [Serializable]
    public class Character {
        protected int level;
        protected float health;
        protected string name;

        public Character(int _level, float _health, string _name) {
            level = _level;
            health = _health;
            name = _name;
        }

        public virtual void Hi() {

        }
    }
}